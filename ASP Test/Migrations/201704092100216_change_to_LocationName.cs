namespace ASP_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_to_LocationName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "LocationName", c => c.String());
            DropColumn("dbo.Items", "LocationID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "LocationID", c => c.Int());
            DropColumn("dbo.Items", "LocationName");
        }
    }
}
