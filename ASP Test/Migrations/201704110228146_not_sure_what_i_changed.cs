namespace ASP_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class not_sure_what_i_changed : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "Created", c => c.DateTime());
            AlterColumn("dbo.Items", "Updated", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "Updated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Items", "Created", c => c.DateTime(nullable: false));
        }
    }
}
