namespace ASP_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_back_to_LocationID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "LocationID", c => c.Int());
            AddColumn("dbo.Locations", "Name", c => c.String());
            DropColumn("dbo.Items", "LocationName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "LocationName", c => c.String());
            DropColumn("dbo.Locations", "Name");
            DropColumn("dbo.Items", "LocationID");
        }
    }
}
