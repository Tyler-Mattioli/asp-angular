namespace ASP_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FromTheBeginning : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Quantity = c.Int(),
                        Expires = c.DateTime(),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        LocationID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Locations");
            DropTable("dbo.Items");
        }
    }
}
