namespace ASP_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class make_LocationDescription_public : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "Description");
        }
    }
}
