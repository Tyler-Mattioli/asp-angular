﻿using System;
using System.ComponentModel.DataAnnotations;
using DAL.Models.DB;

namespace ASP_Test.Models.DB
{
    public class Item : IEntity
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? Quantity { get; set; }

        public DateTime? Expires { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Updated { get; set; }

        public int? LocationID { get; set; }
    }
}