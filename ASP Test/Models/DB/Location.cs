﻿using DAL.Models.DB;
using System.ComponentModel.DataAnnotations;

namespace ASP_Test.Models.DB
{
    public class Location : IEntity
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}