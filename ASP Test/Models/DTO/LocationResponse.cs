﻿using ASP_Test.Models.DB;
using DAL.Models.DTO;

namespace ASP_Test.Models.DTO
{
    public class LocationResponse : IResponse<Location>
    {
        public int ID;
        public string Name;
        public string Description;

        public void ParseExisting(Location existing)
        {
            ID = existing.ID;
            Name = existing.Name;
            Description = existing.Description;
        }
    }
}