﻿using ASP_Test.Models.DB;
using DAL.Models.DTO;

namespace ASP_Test.Models.DTO
{
    public class LocationRequest : IRequest<Location>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Location ToDB()
        {
            return new Location
            {
                Description = this.Description,
                Name = this.Name
            };
        }
    }
}