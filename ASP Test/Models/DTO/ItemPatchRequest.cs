﻿using ASP_Test.Models.DB;
using DAL.Models.DTO;
using System;

namespace ASP_Test.Models.DTO
{
    public class ItemPatchRequest : IPatchRequest<Item>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public DateTime? Expires { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public int? LocationID { get; set; }

        public Item ToDB(Item existing)
        {
            existing.Name = Name ?? existing.Name;
            existing.Description = Description ?? existing.Description;
            existing.Quantity = Quantity ?? existing.Quantity;
            existing.Expires = Expires ?? existing.Expires;
            existing.Created = Created ?? existing.Created;
            existing.Updated = System.DateTime.Now;
            existing.LocationID = LocationID ?? existing.LocationID;

            return existing;
        }
    }
}