﻿using System;
using ASP_Test.Models.DB;
using DAL.Models.DTO;

namespace ASP_Test.Models.DTO
{
    public class ItemRequest : IRequest<Item>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public DateTime? Expires { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public int? LocationID { get; set; }

        public Item ToDB()
        {
            return new Item
            {
                Name = this.Name,
                Description = this.Description,
                Quantity = this.Quantity ?? 0,
                Expires = this.Expires,
                Created = System.DateTime.Now,
                Updated = System.DateTime.Now,
                LocationID = this.LocationID,
            };
        }
    }
}