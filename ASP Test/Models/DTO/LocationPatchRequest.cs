﻿using ASP_Test.Models.DB;
using DAL.Models.DTO;

namespace ASP_Test.Models.DTO
{
    public class LocationPatchRequest : IPatchRequest<Location>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Location ToDB(Location existing)
        {
            existing.Name = Name ?? existing.Name;
            existing.Description = Description ?? existing.Description;

            return existing;
        }
    }
}