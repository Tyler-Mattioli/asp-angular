﻿using ASP_Test.Models;
using ASP_Test.Models.DB;
using ASP_Test.Models.DTO;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ASP_Test.Controllers.API
{
    public class LocationController : ApiController
    {

        private ApplicationDBContext database { get; set; }
        private EntityRepository<Location> location_db;
        
        public LocationController()
        {
            database = new ApplicationDBContext();
            location_db = new EntityRepository<Location>(database);
        }

        public class Body
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public int? ID { get; set; }
        }

        [HttpGet]
        [ResponseType(typeof(Location))]
        public async Task<IHttpActionResult> GetLocation(int id)
        {
            Location results = await database.Set<Location>().FindAsync(id);
            if (results == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(results);
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<Location>))]
        public async Task<IHttpActionResult> GetLocations(int page = 1, int take = 10)
        {
            PaginatedList<Location> existing = await location_db.Paginate(
                page,
                take,
                o => o.ID);

            return Ok(existing.ToPaginatedDto<LocationResponse, Location>());
        }

        

        [HttpPost]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> CreateLocation(LocationRequest request)
        {
            Location dbObj = request.ToDB();

            location_db.Add(dbObj);
            await location_db.Save();

            return CreatedAtRoute("DefaultApi",
                new
                {
                    ID = dbObj.ID
                },
                dbObj);
        }

        [HttpPatch]
        public async Task<IHttpActionResult> UpdateLocation(int id, LocationPatchRequest request)
        {
            Location dbObj = await location_db.GetSingleOrDefaultWhere(o => o.ID == id);

            if (dbObj == null)
                return NotFound();

            dbObj = request.ToDB(dbObj);

            location_db.Edit(dbObj);
            await location_db.Save();

            return StatusCode(System.Net.HttpStatusCode.NoContent);

        }

        [HttpDelete]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> DeleteLocation(int id)
        {
            IQueryable<Item> queryableItems = database.Set<Item>().AsQueryable().Where<Item>(o => o.LocationID == id);
            await queryableItems.LoadAsync();

            foreach (Item i in queryableItems)
            {
                i.LocationID = null;
            }


            Location location = await database.Set<Location>().FindAsync(id);
            if (location == null)
            {
                return NotFound();
            }
            database.Set<Location>().Remove(location);
            database.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // From: http://www.codelocker.net/96/c-sharp-dot-net-take-a-list-of-any-object-and-paginate-it/
        public static List<T> GetPaginatedList<T>(IList<T> source, int page, int recordsPerPage)
        {
            int startIndex = 0;
            int endIndex = 0;

            if (page <= 0)
            {
                page = 1;
            }

            if (recordsPerPage <= 0)
            {
                startIndex = 0;
                endIndex = Int32.MaxValue;
            }
            else
            {
                startIndex = (page * recordsPerPage) - recordsPerPage;
                endIndex = (page * recordsPerPage) - 1;
            }

            //Cap end Index
            if (endIndex > source.Count - 1)
            {
                endIndex = source.Count - 1;
            }

            List<T> newList = new List<T>();
            for (int x = startIndex; x <= endIndex; x++)
            {
                newList.Add((T)source[x]);
            }

            return newList;
        }
    }
}
