﻿using ASP_Test.Models;
using ASP_Test.Models.DB;
using ASP_Test.Models.DTO;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DAL;
using DAL.Models;

namespace ASP_Test.Controllers.API
{
    public class ItemController : ApiController
    {

        private ApplicationDBContext database { get; set; }
        private EntityRepository<Item> item_db;

        public ItemController()
        {
            database = new ApplicationDBContext();
            item_db = new EntityRepository<Item>(database);
        }

        public class Body
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public DateTime? Expires { get; set; }
            public int? Quantity { get; set; }
            public int? LocationID { get; set; }
        }

        [HttpGet]
        [ResponseType(typeof(Item))]
        public async Task<IHttpActionResult> GetItem(int id)
        {
            Item existing = await item_db.GetSingleOrDefaultWhere(o => o.ID == id);
            if(existing == null)
            {
                return NotFound();
            }
            else
            {
                ItemResponse resp = new ItemResponse();
                resp.ParseExisting(existing);
                return Ok(resp);
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<Item>))]
        public async Task<IHttpActionResult> GetItems(int page = 1, int take = 10)
        {
            PaginatedList<Item> existing = await item_db.Paginate(
                page,
                take,
                o => o.ID);

            return Ok(existing.ToPaginatedDto<ItemResponse, Item>());
        }
        
        [HttpPost]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> CreateItem(ItemRequest request)
        {
            Item dbObj = request.ToDB();

            item_db.Add(dbObj);
            await item_db.Save();

            return CreatedAtRoute("DefaultApi",
                new
                {
                    ID = 5
                },
                dbObj);
        }

        [HttpPatch]
        public async Task<IHttpActionResult> UpdateItem(int id, ItemPatchRequest request)
        {
            Item dbObj = await item_db.GetSingleOrDefaultWhere(o => o.ID == id);

            if (dbObj == null)
                return NotFound();

            dbObj = request.ToDB(dbObj);

            item_db.Edit(dbObj);
            await item_db.Save();

            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> DeleteItem(int id)
        {
            Item item = await database.Set<Item>().FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            database.Set<Item>().Remove(item);
            database.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }

    }
}
